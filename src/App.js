import React from 'react';

import {
    Switch,
    Route,
    HashRouter
  } from "react-router-dom";
import { connect } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import { DashBoard } from './pages';


function Routing( props )
{
    return (
        <HashRouter>
            <div>
                <Switch>
                    <Route exact path="/">
                        <DashBoard />
                    </Route>
                    
                </Switch>
            
            </div>
        </HashRouter>
    )
}


const mapSateToProps = ( state ) => {
    return {
        
    }
}

export default connect( mapSateToProps, null )( Routing )