import React from 'react';

import './style.scss';

const Sidebar = props => {
    return (
        <div className= 'side-bar-container'>
            <div className = 'side-bar-brand'>
                Online Course
            </div>
            <div className = 'navs'>
                <div className = 'title'>
                    categories
                </div>
                <div className = 'menu'>
                    <li>item 1</li>
                    <li>item 1</li>
                </div>
            </div>
        </div>
    )
}

export default Sidebar;