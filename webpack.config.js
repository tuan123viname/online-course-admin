const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const   VENDOR_LIRS = [
    'react',
    'react-dom',
    '@fortawesome/free-brands-svg-icons',
    '@fortawesome/free-regular-svg-icons',
    '@fortawesome/react-fontawesome',
    '@fortawesome/fontawesome-svg-core',
    '@fortawesome/free-solid-svg-icons',
    'react-router-dom',
    'react-star-ratings',
    'redux',
    'react-redux',
    'redux-saga',
    'video-react',
    '@material-ui/core',
    '@material-ui/icons',
    '@material-ui/styles',
    '@material-ui/lab'
]

module.exports = {
    entry : {
        bundle : './src/index.js',
        vendors : VENDOR_LIRS
    },
    output : {
        path : path.join(__dirname , 'dist'),
        filename : '[name].[hash].js'
    },
    module : {
        rules : [
            {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
            loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use:  ['style-loader','css-loader']
        
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
                
            },
            //loader image
            {
                test : /\.(png|jpg|svg|gif)$/,
                use :['file-loader']
            },
            //loader font
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                  'file-loader',
                ],
              },
        ]
    },
    plugins : [
        new HtmlWebpackPlugin({
            title: 'config',
            filename : 'index.html',
            template : './src/index.html'
        }),
        new CleanWebpackPlugin(),
    ],
    
    optimization: {
        splitChunks: {
            //phan tach cac thu vien vendor ra 1 file rieng, bundle rieng
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name : 'vendors',
              chunks: 'all'
            }
          }
        }
      }
}